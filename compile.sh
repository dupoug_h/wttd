DIR_SOURCES="."
DIR_INCLUDES="."
clear
rm -f WttD
if [ -d sources ]
then
    DIR_SOURCES="sources"
fi
if [ -d includes ]
then
    DIR_INCLUDES="includes"
fi
gcc ${DIR_SOURCES}/*.c -g3 -W -Wall -Wextra -Werror -I ${DIR_INCLUDES} -o WttD && valgrind ./WttD
