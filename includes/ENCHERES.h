#ifndef ENCHERES_H
#define ENCHERES_H

#include <stdio.h>
#include <stdlib.h>

#include "MISE_EN_PLACE.h"
#include "ENCHERES.h"
#include "MONSTRE.h"
#include "DONJON.h"
#include "AVENTURIER.h"

void    lance_les_encheres(Joueur* joueur_qui_joue, Donjon* donjon, PiocheMonstre* pioche_monstre, Aventurier* aventurier);
void    action_de_passer_le_tour(Joueur* joueur_qui_joue);
void    action_de_pioche_une_carte(Joueur* joueur_qui_joue, Donjon* donjon, PiocheMonstre* pioche_monstre, Aventurier* aventurier);
void    mettre_carte_dans_le_donjon(Joueur* joueur_qui_joue, Donjon* donjon);
void    retirez_monstre_et_equipement(Joueur* joueur_qui_joue, Aventurier* aventurier);

#endif
