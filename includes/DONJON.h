#ifndef DONJON_H
#define DONJON_H

#define MONSTRE_POSSIBLE_DANS_UN_DONJON     NOMBRE_DE_MONSTRES
#define DONJON_EN_COURS_DE_PARTIE           1
#define DONJON_FIN_DE_PARTIE                0

#include "AVENTURIER.h"
#include "MONSTRE.h"
#include "PIOCHE_MONSTRE.h"

typedef struct      S_Donjon
{
    Monstre*        cartes_monstres[TAILLE_MAX_NOM_DU_MONSTRE];
    unsigned int    puissance_du_donjon;
    unsigned int    statut_du_donjon;

}                   Donjon;

void                initialiser_le_donjon(Donjon* donjon);
void                ajouter_un_monstre_au_donjon(Donjon* donjon, Monstre* monstre);
void                afficher_les_monstres_du_donjon(Donjon* donjon);
int                 selectionner_les_effets(Donjon* donjon, Aventurier* aventurier, PiocheMonstre* pioche_monstre);
void                calcul_puissance_du_donjon(Donjon* donjon);
void                effet_anneau_de_pouvoir(Donjon* donjon);
void                effet_cape_invisibilite(Donjon* donjon);
void                effet_dague_vorpale_epee_vorpale(Donjon* donjon);
void                effet_hache_vorpale(Donjon* donjon);
void                effet_marteau_de_guerre(Donjon* donjon);
void                effet_torche(Donjon* donjon);
void                effet_pacte_avec_le_demon(Donjon* donjon);
int                 effet_omnipotence(Donjon* donjon);
void                effet_polymorphe(Donjon* donjon, PiocheMonstre* pioche_monstre);
void                effet_saint_graal(Donjon* donjon);
void                effet_lance_dragon(Donjon* donjon);
void                effet_potion_de_soin(Donjon* donjon);
void                action_de_quitter_selection_des_effets();

#endif // DONJON_H
