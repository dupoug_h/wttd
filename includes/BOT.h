#ifndef BOT_H
#define BOT_H

#include "JOUEUR.h"
#include "AVENTURIER.h"
#include "DONJON.h"
#include "PIOCHE_MONSTRE.h"
#include "ENCHERES.h"
#include "CONSOLE.h"

void        initialiser_le_bot(Joueur* bot, unsigned int position_du_joueur);
void        executer_le_bot(Donjon* donjon, Joueur* bot, PiocheMonstre* pioche_monstre, Aventurier* aventurier);
void        piocher_une_carte_bot(PiocheMonstre* pioche_monstre, Joueur* bot);
void        choix_action_apres_pioche_bot(Donjon* donjon, Joueur* joueur_qui_joue, Aventurier* aventurier);
void        noms_des_bots(char* nom_du_bot);
int         bot_du_donjon(Donjon* donjon, Aventurier* aventurier, PiocheMonstre* pioche_monstre);
void        retirez_monstre_et_equipement_bot(Aventurier* aventurier);
void        retirer_un_equipement_de_aventurier_bot(Aventurier* aventurier);
void        retirer_un_effet_de_aventurier_bot(Aventurier* aventurier);

#endif // BOT_H
