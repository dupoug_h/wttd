#ifndef MISE_EN_PLACE_H
#define MISE_EN_PLACE_H

#include "AVENTURIER.h"
#include "JOUEUR.h"

void            demander_le_pseudo_de_chaque_joueurs(unsigned int nombre_de_joueurs, Joueur* joueurs);
Aventurier*     choisir_un_aventurier_pour_aller_au_donjon(Aventurier* aventuriers);
void            afficher_le_welcome();
void            afficher_message_de_felicitations_fin_de_partie();
void            afficher_vous_entrer_dans_le_donjon();
#endif // MISE_EN_PLACE_H
