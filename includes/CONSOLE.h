#ifndef CONSOLE_H
#define CONSOLE_H

#define ERROR_WHEN_READING_FROM_CONSOLE  0xffffffff

void            initialiser_la_console();
unsigned int	lire_un_nombre_depuis_la_console(const char*  message_de_demande, const char*  message_invalide, unsigned int valeur_minimale, unsigned int valeur_maximale);
void            effacer_la_console();
#endif
