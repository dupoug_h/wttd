#ifndef MONSTRE_H
#define MONSTRE_H

#define NOMBRE_DE_MONSTRES          13
#define TAILLE_MAX_NOM_DU_MONSTRE   32

// Type monstre
typedef struct  S_Monstre
{
    int         puissance;
    char        nom[TAILLE_MAX_NOM_DU_MONSTRE];
}               Monstre;

void            initialiser_les_monstres(Monstre* monstres);
void            afficher_la_carte_monstre(Monstre* monstre);

#endif // MONSTRE_H
