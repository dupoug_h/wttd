#ifndef JOUEUR_H
#define JOUEUR_H

#include <stdlib.h>
#include <unistd.h>

#include "MONSTRE.h"
#include "PIOCHE_MONSTRE.h"
#include "DONJON.h"

#define TAILLE_MAX_NOM_DU_JOUEUR 32
#define JOUEUR_A_PASSER          1
#define JOUEUR_JOUE_ENCORE       2
#define NOMBRE_DE_JOUEURS_MAX    4
#define NOMBRE_DE_JOUEURS_MIN    1
#define NOMBRE_DE_VICTOIRES_MAX  2
#define JE_SUIS_UN_BOT           0
#define JE_NE_SUIS_PAS_UN_BOT    1

typedef struct      s_Joueur
{
    char            pseudo[TAILLE_MAX_NOM_DU_JOUEUR];
    unsigned int    id;                                  //Num�ro du joueur
    unsigned int    nombre_de_victoires_du_donjon;
    unsigned int    passer_le_tour;
    unsigned int    suis_je_un_bot;
    Monstre*        monstre_dans_ma_main;
}                   Joueur;

void                joueur_tire_une_carte_monstre(Joueur* moi, PiocheMonstre* pioche);
int                 il_y_a_t_il_encore_des_joueurs_qui_peuvent_jouer(Joueur* joueurs, unsigned int nombre_de_joueurs_qui_joue);
Joueur*             joueur_qui_joue_encore(Joueur* joueurs, unsigned int nombre_de_joueurs_qui_joue);
int                 verification_des_victoires_des_joueurs(Joueur* joueurs, unsigned int nombre_de_joueurs);
void                victoire_du_donjon (Joueur* joueur, unsigned int nombre_de_joueurs);
void                defaite_du_donjon(Joueur* joueurs, unsigned int nombre_de_joueurs);
void                comparer_points_de_vie_aventurier_donjon(Donjon* donjon, Aventurier* aventurier, Joueur* joueurs, unsigned int nombre_de_joueurs);
int                 fin_de_partie(Donjon* donjon, Joueur* joueurs, unsigned int nombre_de_joueurs);
void                initialiser_les_joueurs(Joueur* joueur, char* nom_du_joueur, unsigned int position_du_joueur);
void                reinitialiser_les_joueurs_qui_ne_jouaient_plus(Joueur* joueurs);

#endif // JOUEUR_H
