#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "JOUEUR.h"
#include "PIOCHE_MONSTRE.h"
#include "DONJON.h"
#include "EFFETS.h"
#include "MISE_EN_PLACE.h"

void    joueur_tire_une_carte_monstre(Joueur* moi, PiocheMonstre* pioche_monstre)
{
    moi->monstre_dans_ma_main = piocher_un_monstre(pioche_monstre);
}

int     il_y_a_t_il_encore_des_joueurs_qui_peuvent_jouer(Joueur* joueurs, unsigned int nombre_de_joueurs_qui_joue)
{
    unsigned int nombre_de_joueurs_qui_joue_encore = 0;

    for (unsigned int index_joueur_en_jeu = 0; index_joueur_en_jeu < nombre_de_joueurs_qui_joue; index_joueur_en_jeu++)
    {
        if (joueurs[index_joueur_en_jeu].passer_le_tour == JOUEUR_JOUE_ENCORE)
            nombre_de_joueurs_qui_joue_encore++;
    }
    if (nombre_de_joueurs_qui_joue_encore <= 1)
        return (0);
    else
        return (666);
}

Joueur* joueur_qui_joue_encore(Joueur* joueurs, unsigned int nombre_de_joueurs_qui_joue)
{
    for (unsigned int index_joueur_en_jeu = 0; index_joueur_en_jeu < nombre_de_joueurs_qui_joue; index_joueur_en_jeu++)
    {
        if (joueurs[index_joueur_en_jeu].passer_le_tour == JOUEUR_JOUE_ENCORE)
            return (&(joueurs[index_joueur_en_jeu]));
    }
    return (NULL);
}

int     verification_des_victoires_des_joueurs(Joueur* joueurs, unsigned int nombre_de_joueurs)
{
      for (unsigned int index_nombre_de_joueur_en_jeu = 0; index_nombre_de_joueur_en_jeu < nombre_de_joueurs; index_nombre_de_joueur_en_jeu++)
      {
        for (unsigned int index_nombre_victoires_des_joueurs = 0; index_nombre_victoires_des_joueurs < NOMBRE_DE_VICTOIRES_MAX; index_nombre_victoires_des_joueurs++)
        {
            if (joueurs[index_nombre_de_joueur_en_jeu].nombre_de_victoires_du_donjon == NOMBRE_DE_VICTOIRES_MAX)
            return (DONJON_FIN_DE_PARTIE);
        }
      }
      return (DONJON_EN_COURS_DE_PARTIE);
}

void    comparer_points_de_vie_aventurier_donjon(Donjon* donjon, Aventurier* aventurier, Joueur* joueurs, unsigned int nombre_de_joueurs)
{
    if (donjon->puissance_du_donjon <= aventurier->PV)
    {
        for (unsigned int index_chercher_potion_de_soin = 0; index_chercher_potion_de_soin < NOMBRE_MAX_EFFETS; index_chercher_potion_de_soin++)
        {
            if (strcmp(aventurier->effets[index_chercher_potion_de_soin]->nom, "potion de soin") == 0)
            {
                initialiser_les_pv_un_aventurier(aventurier);
                return ;
            }
        }
        defaite_du_donjon(joueurs, nombre_de_joueurs);
    }
    else
        victoire_du_donjon(joueurs, nombre_de_joueurs);
}

void    defaite_du_donjon (Joueur* joueurs, unsigned int nombre_de_joueurs)
{
    printf(" %s avez perdu ce donjon, on commence une nouvelle manche\n", joueur_qui_joue_encore(joueurs, nombre_de_joueurs)->pseudo);
}

void    victoire_du_donjon (Joueur* joueurs, unsigned int nombre_de_joueurs)
{
    printf("F�licitations, %s vous avez remport� le donjon et une carte r�ussite ! \n\n",joueur_qui_joue_encore(joueurs, nombre_de_joueurs)->pseudo);
    joueur_qui_joue_encore(joueurs, nombre_de_joueurs)->nombre_de_victoires_du_donjon++;
}

int     fin_de_partie(Donjon* donjon, Joueur* joueurs, unsigned int nombre_de_joueurs)
{
    afficher_message_de_felicitations_fin_de_partie();
    printf(" %s a remport� cette partie de Welcome to the Dungeon, toutes nos f�licitations, tu as su battre et terraser ces ignobles cr�atures\n Merci d'avoir jou� et � tr�s bientot\n",joueur_qui_joue_encore(joueurs, nombre_de_joueurs)->pseudo);
    donjon->statut_du_donjon = DONJON_FIN_DE_PARTIE;
    return(donjon->statut_du_donjon);
}

void    initialiser_les_joueurs(Joueur* joueur, char* nom_du_joueur, unsigned int position_du_joueur)
{
    joueur->id = position_du_joueur;
    joueur->nombre_de_victoires_du_donjon = 0;
    joueur->passer_le_tour = JOUEUR_JOUE_ENCORE;
    joueur->suis_je_un_bot = JE_NE_SUIS_PAS_UN_BOT;
    strcpy(joueur->pseudo, nom_du_joueur);
}

void                reinitialiser_les_joueurs_qui_ne_jouaient_plus(Joueur* joueurs)
{
    for (unsigned int joueur_index = 0; joueur_index != NOMBRE_DE_JOUEURS_MAX; joueur_index++)
        joueurs[joueur_index].passer_le_tour = JOUEUR_JOUE_ENCORE;
}
