#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "CONSOLE.H"

#if defined(__unix__) || defined(__unix) || defined(unix) || defined(__linux__) || defined(__FreeBSD__)

void            initialiser_la_console()
{
    return ;
}
void effacer_la_console()
{
    return ;
}

unsigned int	lire_un_nombre_depuis_la_console(const char*  message_de_demande,
                                                 const char*  message_invalide,
                                                 unsigned int valeur_minimale,
                                                 unsigned int valeur_maximale)

{
    unsigned int    nombre      = 0;
    unsigned int    nb_chars	= 0;
    unsigned int    curseur     = 0;
    char	        reponse[8]	= "";

    if (valeur_maximale < 10)
        nb_chars = 1;
    else if (valeur_maximale < 100)
        nb_chars = 2;
    else if (valeur_maximale < 1000)
        nb_chars = 3;
    else if (valeur_maximale < 10000)
        nb_chars = 4;
    else
        return (ERROR_WHEN_READING_FROM_CONSOLE);
    while (42)
    {
        if (sleep(1) > 2)
            return (ERROR_WHEN_READING_FROM_CONSOLE);
        if (write(1, message_de_demande, strlen(message_de_demande)) <= 0)
            return (ERROR_WHEN_READING_FROM_CONSOLE);
        reponse[0] = '\0';
        reponse[1] = '\0';
        reponse[2] = '\0';
        reponse[3] = '\0';
        reponse[4] = '\0';
        reponse[5] = '\0';
        reponse[6] = '\0';
        reponse[7] = '\0';
        curseur = 0;
        while (curseur < 8)
        {
            if (read(1, reponse + curseur, 1) == 1)
                curseur++;
            else
                return (ERROR_WHEN_READING_FROM_CONSOLE);
            if (reponse[curseur - 1] == '\n')
              break ;
        }
        if (curseur - 1 > nb_chars)
        {
            if (write(1, message_invalide, strlen(message_invalide)) <= 0)
                return (ERROR_WHEN_READING_FROM_CONSOLE);
            return (ERROR_WHEN_READING_FROM_CONSOLE);
        }
        else
        {
            nombre = 0;
            curseur = 0;
            while (reponse[curseur] != '\n')
            {
                nombre = (nombre * 10) + (reponse[curseur] - '0');
                curseur++;
            }
            if ((nombre >= valeur_minimale) && (nombre <= valeur_maximale))
              return (nombre);
            else
            {
                if (write(1, message_invalide, strlen(message_invalide)) <= 0)
                    return (ERROR_WHEN_READING_FROM_CONSOLE);
            }
        }
    }
    return (ERROR_WHEN_READING_FROM_CONSOLE);
}

#elif defined(_WIN32) || defined(_WIN64)

#include <locale.h>
#include <windows.h>

void            initialiser_la_console()
{
    SetConsoleCP(1252);
    SetConsoleOutputCP(1252);
}

unsigned int	    lire_un_nombre_depuis_la_console(const char*  message_de_demande,
						     const char*  message_invalide,
						     unsigned int valeur_minimale,
						     unsigned int valeur_maximale)
{
    int		    code_erreur = 0;
    unsigned int    nombre = 0;

    while (42)
    {
	Sleep(1000);
	if (write(1, message_de_demande, strlen(message_de_demande)) <= 0)
	    return (ERROR_WHEN_READING_FROM_CONSOLE);
	if ((code_erreur = scanf("%u", &nombre)) == 1)
	{
	    fflush(stdin);
	    if ((nombre >= valeur_minimale) && (nombre <= valeur_maximale))
	      return (nombre);
	    else
	    {
		if (write(1, message_invalide, strlen(message_invalide)) <= 0)
		    return (ERROR_WHEN_READING_FROM_CONSOLE);
	    }
	}
	else
	{
	    fflush(stdin);
	    if (write(1, message_invalide, strlen(message_invalide)) <= 0)
		return (ERROR_WHEN_READING_FROM_CONSOLE);
	}
    }
    return (ERROR_WHEN_READING_FROM_CONSOLE);
}

void effacer_la_console()
{
   system("cls");
   return;
}

#endif
