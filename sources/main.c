#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "JOUEUR.h"
#include "MISE_EN_PLACE.h"
#include "MONSTRE.h"
#include "EQUIPEMENTS.h"
#include "EFFETS.h"
#include "AVENTURIER.h"
#include "PIOCHE_MONSTRE.h"
#include "ENCHERES.h"
#include "CONSOLE.h"
#include "BOT.h"

int                 lancer_le_jeu()
{
    Joueur           joueurs[NOMBRE_DE_JOUEURS_MAX];
    Monstre          monstres[NOMBRE_DE_MONSTRES];
    Equipement       equipements[NOMBRE_DES_EQUIPEMENTS];
    Effet            effets[NOMBRE_D_EFFETS];
    Aventurier       aventuriers[NOMBRE_D_AVENTURIERS];
    PiocheMonstre    pioche_monstre;
    Donjon           donjon;

    initialiser_les_monstres(monstres);
    initialiser_les_equipements(equipements);
    initialiser_les_effets(effets);
    initialiser_les_aventuriers(aventuriers, effets, equipements);
    initialiser_la_pioche(&pioche_monstre, monstres);
    initialiser_le_donjon(&donjon);

    unsigned int nombre_de_joueurs = ERROR_WHEN_READING_FROM_CONSOLE;
    while ((nombre_de_joueurs = lire_un_nombre_depuis_la_console("Saisissez le nombre de joueurs: ", "Le nombre de joueurs n'est pas valide\n", NOMBRE_DE_JOUEURS_MIN, NOMBRE_DE_JOUEURS_MAX)) == ERROR_WHEN_READING_FROM_CONSOLE);
    demander_le_pseudo_de_chaque_joueurs(nombre_de_joueurs, joueurs);
    if (nombre_de_joueurs == 1 )
    {
        nombre_de_joueurs++;
        initialiser_le_bot(&(joueurs[1]), 1);
    }
    effacer_la_console();
    printf("\n\nLES ENCHERES COMMENCENT !!!\n\n");
    while(verification_des_victoires_des_joueurs (joueurs, nombre_de_joueurs))
    {
        initialiser_les_aventuriers(aventuriers, effets, equipements);
        Aventurier*  choix_aventurier = choisir_un_aventurier_pour_aller_au_donjon(aventuriers);
        if (choix_aventurier == NULL)
        {
            printf("Le choix du nombre de joueurs n'est pas valide :-)\n");
            return (-1);
        }
        afficher_la_carte_aventurier(choix_aventurier);
        reinitialiser_les_joueurs_qui_ne_jouaient_plus(joueurs);
        for (unsigned int joueur_choisi = 0; il_y_a_t_il_encore_des_joueurs_qui_peuvent_jouer(joueurs, nombre_de_joueurs); joueur_choisi++)
        {
            if (joueur_choisi == nombre_de_joueurs)
                joueur_choisi = 0;
            if (joueurs[joueur_choisi].passer_le_tour == JOUEUR_JOUE_ENCORE)
            {
                printf("Je suis le joueur %s \n",joueurs[joueur_choisi].pseudo);
                lance_les_encheres(&joueurs[joueur_choisi], &donjon, &pioche_monstre, choix_aventurier);
            }
            else if (joueurs[joueur_choisi].passer_le_tour == JOUEUR_A_PASSER)
            {
                printf("%s, vous avez eu peur de continuer cette aventure.\nVous ne pouvez donc plus jouer lors de cette manche\n\n", joueurs[joueur_choisi].pseudo);
            }
            effacer_la_console();
        }
        afficher_vous_entrer_dans_le_donjon();
        printf("\nVos camarades ont abandonn� les ench�res, %s vous �tes d�sormais face au donjon\n", joueur_qui_joue_encore(joueurs, nombre_de_joueurs)->pseudo);
        printf("\n");
        afficher_les_monstres_du_donjon(&donjon);
        printf("\n");
        afficher_les_equipements_de_aventurier(choix_aventurier);
        if (selectionner_les_effets(&donjon, choix_aventurier, &pioche_monstre) == 1)
            victoire_du_donjon(joueurs, nombre_de_joueurs);
        else
        {
            calcul_puissance_du_donjon(&donjon);
            calcul_pv_aventurier_au_donjon(choix_aventurier);
            comparer_points_de_vie_aventurier_donjon(&donjon, aventuriers, joueur_qui_joue_encore(joueurs, nombre_de_joueurs), nombre_de_joueurs);
        }
    }
    return (0);
}

int            debut_de_partie()
{
 unsigned int    choix_action = 0;

    printf(" ___________  Que voulez-vous faire ?  ___________________________________________ \n");
    printf("|                                                                                 |\n");
    printf("|   1. Commencer une partie                                                       |\n");
    printf("|   2. Afficher les r�gles                                                        |\n");
    printf("|   3. Quitter le jeu                                                             |\n");
    printf("|_________________________________________________________________________________|\n");
    while(42)
    {
    choix_action = lire_un_nombre_depuis_la_console("Choisissez votre action : ", "Le choix n'est pas valide\n", 1, 3);
    printf("\n");
        switch (choix_action)
        {
            case 1 : return(lancer_le_jeu());
                break;
            case 2 : printf("\"les r�gles\" ;)\n\n\n");
                     sleep (5);
                     printf("- Choisissez ou tirez al�atoirement un Aventurier et placez-le au centre de la table.\n");
                     printf("Le jeu se d�roule en plusieurs manches durant lesquelles vous allez ench�rir pour savoir qui ira dans le Donjon.\n");
                     printf(" � l�issue de chaque manche, un joueur entrera dans le Donjon, se rapprochant de la victoire ou de la d�faite en fonction de sa r�ussite\n");
                     printf("Une manche est compos�e de deux phases :  . Une phase d�ench�re . Une phase de Donjon.\n");
                     printf("Phase d�ench�re \n\n");
                     printf("Durant cette phase, vous jouez � tour de r�le dans le sens des aiguilles d�une montre. Durant votre tour, vous avez le choix entre \n");
                     printf(". Piocher une carte\n\n");
                     printf("Passer votre tour et ne plus participer � la manche. Vous ne jouez plus avant la manche suivante.\n\n");
                     printf("Piocher une carteSi vous piochez une carte, prenez la premi�re carte de la pioche montrer � qui que ce soit.Vous avez alors le choix entre\n ");
                     printf("1. Rajouter le Monstre au Donjon. Dans ce cas, vous posez la carte Monstre face cach�e sur le Donjon. La pile ainsi form�e contient les Monstres que vous devrez affronter si vous entrez dans le Donjon.\n ");
                     printf("2. Poser le Monstre devant vous face cach�e. Dans ce cas, vous devez retirer l��quipement de votre choix qui se trouve encore sous l�Aventurier et le poser sur le Monstre devant vous.La carte Monstre et l��quipement sont ainsi �cart�s pour toute la dur�e de la manche\n");
                     printf("Fin de la phase d�ench�reQuand tous les joueurs ont pass� sauf un, la phase d�ench�re prend fin et ce dernier doit affronter seul le Donjon avec les �quipements qui sont encore disponibles sous l�Aventurier. Il passe � la phase Donjon.\n\n");
                     printf("Phase Donjon : Seul le joueur qui n�a pas pass� participe � cette phase. Commencez par calculer votre score total de points de vie (PV) en additionnant la valeur de la tuile Aventurier et de tous les �quipements lui apportant des PV. \n");
                     printf(". Si le total des d�g�ts inflig�s par les Monstres est sup�rieur ou �gal aux points de vie de d�part de l�Aventurier, le d�fi n�est pas r�ussi.\n");
                     printf("La manche est termin�e. Si aucun joueur n�a gagn� la partie, commencez une nouvelle manche.\n\n\n");
                     printf("Nouvelle manche \nM�langez les Monstres et placez-les face cach�e sur la table. Le joueur qui est all� dans le Donjon la manche pr�c�dente choisit l�Aventurier qui sera jou� (Guerrier, Barbare, Mage ou Voleur). Placez-le, ainsi que les �quipements qui lui correspondent au centre de la table \n");
                     printf("Fin de partie Vous gagnez la partie dans deux cas :\n\n");
                     printf(". Vous gagnez une carte R�ussite alors que vous en avez d�j� une devant vous. En d�autres termes, vous avez r�ussi � survivre au Donjon deux fois.\n");
                     printf(". Tous les autres joueurs ont �t� �limin�s de la partie\n\n\n");
                break;
            case 3 : return (0);
                break;
            default : printf("ERREUR : laissez ce pauvre programme tranquille svp\n");
            return (-1);
        }
    }
    return(-1);
}

int main()
{
    initialiser_la_console();
    afficher_le_welcome();
    return (debut_de_partie());
}

