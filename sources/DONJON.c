#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "DONJON.h"
#include "MONSTRE.h"
#include "CONSOLE.h"
#include "PIOCHE_MONSTRE.h"

void    initialiser_le_donjon(Donjon* donjon)
{
    donjon->puissance_du_donjon = 0;
    donjon->statut_du_donjon = DONJON_EN_COURS_DE_PARTIE;
    for (unsigned int index_monstre_donjon = 0; index_monstre_donjon <= MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre_donjon++)
        donjon->cartes_monstres[index_monstre_donjon] = NULL;
}

void    ajouter_un_monstre_au_donjon(Donjon* donjon, Monstre* monstre)
{
    unsigned int    index_monstre_donjon = 0;

    for (index_monstre_donjon = 0; donjon->cartes_monstres[index_monstre_donjon] != NULL; index_monstre_donjon++);
    donjon->cartes_monstres[index_monstre_donjon] = monstre;
}

void    afficher_les_monstres_du_donjon(Donjon* donjon)
{
    printf("Voici les monstres du donjon, il est trop tard pour vous enfuir\n");
    for (unsigned int index_monstre = 0; index_monstre < NOMBRE_DE_MONSTRES; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
            printf("|  %d:-%-25s                         |\n", index_monstre + 1, donjon->cartes_monstres[index_monstre]->nom);
    }
}

int                 selectionner_les_effets(Donjon* donjon, Aventurier* aventurier, PiocheMonstre* pioche_monstre)
{
    unsigned int    choix_effet = 0;

    for (unsigned int index_effet_monstre_a_enlever = 0; index_effet_monstre_a_enlever < NOMBRE_MAX_EFFETS; index_effet_monstre_a_enlever++)
    {
        if (aventurier->effets[index_effet_monstre_a_enlever] != NULL)
        {
            printf(" _______ Voici les diff�rents �quipements, veuillez selectionner les v�tres __ \n");
            printf("|                                                                             |\n");
            printf("|   1.  Anneau de pouvoir                                                     |\n");
            printf("|   2.  Cape d'invisibilit�                                                   |\n");
            printf("|   3.  Dague Vorpale                                                         |\n");
            printf("|   4.  Hache Vorpale                                                         |\n");
            printf("|   5.  Marteau de guerre                                                     |\n");
            printf("|   6.  Torche                                                                |\n");
            printf("|   7.  Pacte avec le d�mon                                                   |\n");
            printf("|   8.  Omnipotence                                                           |\n");
            printf("|   9.  Polymorphe                                                            |\n");
            printf("|   10. Saint graal                                                           |\n");
            printf("|   11. Lance dragon                                                          |\n");
            printf("|   12. Ep�e vorpale                                                          |\n");
            printf("|   13. Vous avez fini d'utiliser vos �quipements                             |\n");
            printf("|_____________________________________________________________________________|\n");
            choix_effet = lire_un_nombre_depuis_la_console("S�lectionnez l'un de vos �quipements: ", "Le choix n'est pas valide\n", 1, 13);
            switch (choix_effet)
            {
                case 1 : effet_anneau_de_pouvoir(donjon);
                    break;
                case 2 : effet_cape_invisibilite(donjon);
                    break;
                case 3 : effet_dague_vorpale_epee_vorpale(donjon);
                    break;
                case 4 : effet_hache_vorpale(donjon);
                    break;
                case 5 : effet_marteau_de_guerre(donjon);
                    break;
                case 6 : effet_torche(donjon);
                    break;
                case 7 : effet_pacte_avec_le_demon(donjon);
                    break;
                case 8 :
                    return (effet_omnipotence(donjon));
                case 9 : effet_polymorphe(donjon, pioche_monstre);
                    break;
                case 10 : effet_saint_graal(donjon);
                    break;
                case 11 : effet_lance_dragon(donjon);
                    break;
                case 12 : effet_dague_vorpale_epee_vorpale(donjon);
                    break;
                case 13 : action_de_quitter_selection_des_effets();
                    break;
                default : printf("ERREUR : laissez ce pauvre programme tranquille svp\n");
            }
        }
    }
    return (0);
}

void    effet_anneau_de_pouvoir(Donjon* donjon)
{
    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
           {

            if (donjon->cartes_monstres[index_monstre]->puissance <= 2)
                 donjon->cartes_monstres[index_monstre] = NULL;
           }
    }
}

void    effet_cape_invisibilite(Donjon* donjon)
{
    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            if (donjon->cartes_monstres[index_monstre]->puissance >= 6)
                donjon->cartes_monstres[index_monstre] = NULL;
        }
    }
}

void    effet_dague_vorpale_epee_vorpale(Donjon* donjon)
{
    unsigned int    index_monstre_enleve = NOMBRE_DE_MONSTRES;
    char            nom_monstre_a_enlever[32];

    afficher_les_monstres_du_donjon(donjon);
    while (index_monstre_enleve == NOMBRE_DE_MONSTRES)
    {
        printf("Ecrivez le nom du monstre que vous souhaitez enlever: ");
        scanf("%s", nom_monstre_a_enlever);
        for (index_monstre_enleve = 0; index_monstre_enleve < NOMBRE_DE_MONSTRES; index_monstre_enleve++)
        {
            if (donjon->cartes_monstres[index_monstre_enleve] != NULL)
            {
                if (strcmp(donjon->cartes_monstres[index_monstre_enleve]->nom, nom_monstre_a_enlever) == 0)
                    break;
            }
        }
    }
    for (index_monstre_enleve = 0; index_monstre_enleve < NOMBRE_DE_MONSTRES; index_monstre_enleve++)
    {
        if (donjon->cartes_monstres[index_monstre_enleve] != NULL)
        {
            if (strcmp(donjon->cartes_monstres[index_monstre_enleve]->nom, nom_monstre_a_enlever) == 0)
            {
                printf("Vous avez enlev� %s du donjon\n", nom_monstre_a_enlever);
                donjon->cartes_monstres[index_monstre_enleve] = NULL;
            }
        }
    }
}

void    effet_hache_vorpale(Donjon* donjon)
{
    unsigned int    index_monstre_enleve = NOMBRE_DE_MONSTRES;
    char            nom_monstre_a_enlever[32];

    afficher_les_monstres_du_donjon(donjon);
    while (index_monstre_enleve == NOMBRE_DE_MONSTRES)
    {
        printf("Ecrivez le nom du monstre que vous souhaitez enlever: ");
        scanf("%s", nom_monstre_a_enlever);
        for (index_monstre_enleve = 0; index_monstre_enleve < NOMBRE_DE_MONSTRES; index_monstre_enleve++)
        {
            if (donjon->cartes_monstres[index_monstre_enleve] != NULL)
            {
                if (strcmp(donjon->cartes_monstres[index_monstre_enleve]->nom, nom_monstre_a_enlever) == 0)
                    break;
            }
        }
    }
    for (index_monstre_enleve = 0; index_monstre_enleve < NOMBRE_DE_MONSTRES; index_monstre_enleve++)
    {
        if (donjon->cartes_monstres[index_monstre_enleve] != NULL)
        {
            if (strcmp(donjon->cartes_monstres[index_monstre_enleve]->nom, nom_monstre_a_enlever) == 0)
            {
                printf("Vous avez enlev� %s du donjon\n", nom_monstre_a_enlever);
                donjon->cartes_monstres[index_monstre_enleve] = NULL;
            }
        }
    }
}

void    effet_marteau_de_guerre(Donjon* donjon)
{
     for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            if (strcmp(donjon->cartes_monstres[index_monstre]->nom, "golem") == 0)
            {
                printf("Vous avez enlev� le golem du donjon\n");
                donjon->cartes_monstres[index_monstre] = NULL;
            }
        }
    }
}

void    effet_torche(Donjon* donjon)
{
    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            if (donjon->cartes_monstres[index_monstre]->puissance <= 3)
                donjon->cartes_monstres[index_monstre] = NULL;
        }
    }
}

void    effet_pacte_avec_le_demon(Donjon* donjon)
{
    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            if (strcmp(donjon->cartes_monstres[index_monstre]->nom, "d�mon") == 0)
            {
                printf("Vous avez enlev� le dragon du donjon\n");
                donjon->cartes_monstres[index_monstre] = NULL;
                for (unsigned int index_monstre_suivant = index_monstre + 1; index_monstre_suivant < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre_suivant++)
                {
                    if (donjon->cartes_monstres[index_monstre_suivant] != NULL)
                    {
                        printf("Le pacte avec le d�mon a supprim� %s du donjon\n", donjon->cartes_monstres[index_monstre_suivant]->nom);
                        donjon->cartes_monstres[index_monstre_suivant] = NULL;
                    }
                }
            }
        }
    }
}

int                effet_omnipotence(Donjon* donjon)
{
    unsigned int    compteur_doublon = 0;

    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            for (unsigned int index_verifier_doublon = 0; index_verifier_doublon < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_verifier_doublon++)
            {
                if (donjon->cartes_monstres[index_verifier_doublon] != NULL)
                {
                    if ((index_monstre != index_verifier_doublon) && (strcmp(donjon->cartes_monstres[index_monstre]->nom, donjon->cartes_monstres[index_verifier_doublon]->nom) == 0))
                    {
                        printf("La carte omnipotence n'a pas d'effets sur le donjon\n");
                        compteur_doublon++;
                    }
                }
            }
        }
    }
    if (compteur_doublon == 0)
    {
        printf("L'effet omnipotence vous a permis de remporter le donjon, F�LICITATIONS !!\n");
        return (1);
    }
    return (0);
}

void    effet_polymorphe(Donjon* donjon, PiocheMonstre* pioche_monstre)
{
    unsigned int index_monstre_enleve = NOMBRE_DE_MONSTRES;
    char        nom_monstre_a_enlever[32];

    afficher_les_monstres_du_donjon(donjon);
    while (index_monstre_enleve == NOMBRE_DE_MONSTRES)
    {
        printf("Ecrivez le nom du monstre que vous souhaitez enlever: ");
        scanf("%s", nom_monstre_a_enlever);
        for (index_monstre_enleve = 0; index_monstre_enleve < NOMBRE_DE_MONSTRES; index_monstre_enleve++)
        {
            if (donjon->cartes_monstres[index_monstre_enleve] != NULL)
            {
                if (strcmp(donjon->cartes_monstres[index_monstre_enleve]->nom, nom_monstre_a_enlever) == 0)
                    break;
            }
        }
    }
    for (index_monstre_enleve = 0; index_monstre_enleve < NOMBRE_DE_MONSTRES; index_monstre_enleve++)
    {
        if (donjon->cartes_monstres[index_monstre_enleve] != NULL)
        {
            if (strcmp(donjon->cartes_monstres[index_monstre_enleve]->nom, nom_monstre_a_enlever) == 0)
            {
                printf("Vous avez enlev� %s du donjon\n", nom_monstre_a_enlever);
                donjon->cartes_monstres[index_monstre_enleve] = piocher_un_monstre(pioche_monstre);
            }
        }
    }
}

void    effet_saint_graal(Donjon* donjon)
{
    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            if (donjon->cartes_monstres[index_monstre]->puissance % 2 == 0)
            {
                printf("Saint Graal a �limin� %s du donjon\n", donjon->cartes_monstres[index_monstre]->nom);
                donjon->cartes_monstres[index_monstre] = NULL;
            }
        }
    }
}

void        effet_lance_dragon(Donjon* donjon)
{
    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
        {
            if (strcmp(donjon->cartes_monstres[index_monstre]->nom, "golem") == 0)
            {
                printf("Vous avez enlev� le dragon du donjon\n");
                donjon->cartes_monstres[index_monstre] = NULL;
            }
        }
    }
}

//On utilise cette fonction uniquement si le joueur poss�de la potion de soin
void        action_de_quitter_selection_des_effets()
{
    return ;
}

void        calcul_puissance_du_donjon(Donjon* donjon)
{
    int     puissance_donjon = 0;

    for (unsigned int index_monstre = 0; index_monstre < MONSTRE_POSSIBLE_DANS_UN_DONJON; index_monstre++)
    {
        if (donjon->cartes_monstres[index_monstre] != NULL)
            puissance_donjon =  puissance_donjon + donjon->cartes_monstres[index_monstre]->puissance;
    }
    printf("La puissance du donjon final est : %d\n\n", puissance_donjon);
}


