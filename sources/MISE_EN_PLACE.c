#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "MISE_EN_PLACE.h"
#include "JOUEUR.h"
#include "AVENTURIER.h"
#include "CONSOLE.h"

void                demander_le_pseudo_de_chaque_joueurs(unsigned int nombre_de_joueurs, Joueur* joueurs)
{
    unsigned int    joueur_id = 0;
    char            joueur_pseudo[TAILLE_MAX_NOM_DU_JOUEUR] = "";

    if (joueurs != NULL)
    {
        while (joueur_id != nombre_de_joueurs)
        {
            printf("\n----Joueur %d : entrez votre pseudo: ", joueur_id + 1);
            scanf("%s", joueur_pseudo);
            initialiser_les_joueurs(&(joueurs[joueur_id]), joueur_pseudo, joueur_id);
            joueur_id++;
        }
        printf("\n");
    }
    else
        printf("ERREUR: demander_le_pseudo_de_chaque_joueurs: joueurs est NULL\n");
}

Aventurier*         choisir_un_aventurier_pour_aller_au_donjon(Aventurier* aventuriers)
{
    unsigned int    choix_aventurier = 0;

    printf(" ___________  Choississez votre aventurier  ______________________________________ \n");
    printf("|                                                                                 |\n");
    printf("|   1. Jouer avec la voleuse                                                      |\n");
    printf("|   2. Jouer avec le barbare                                                      |\n");
    printf("|   3. Jouer avec le mage                                                         |\n");
    printf("|   4. Jouer avec le guerrier                                                     |\n");
    printf("|_________________________________________________________________________________|\n");
    choix_aventurier = lire_un_nombre_depuis_la_console("Choisissez votre aventurier: ", "Le choix n'est pas valide\n", 1, 4);
    printf("\n");
    return (&(aventuriers[choix_aventurier - 1]));
}
void afficher_le_welcome()
{
printf("  _    _ _____ _     _____ ________  ___ _____    _____ _____    _____ _   _  _____   ______ _   _ _   _ _____  _____ _____ _   _ \n");
printf(" | |  | |  ___| |   /  __ \\  _  |  \\/  ||  ___|  |_   _|  _  |  |_   _| | | ||  ___|  |  _  \\ | | | \\ | |  __ \\|  ___|  _  | \\ | |\n");
printf(" | |  | | |__ | |   | /  \\/ | | | .  . || |__      | | | | | |    | | | |_| || |__    | | | | | | |  \\| | |  \\/| |__ | | | |  \\| |\n");
printf(" | |/\\| |  __|| |   | |   | | | | |\\/| ||  __|     | | | | | |    | | |  _  ||  __|   | | | | | | | . ` | | __ |  __|| | | | . ` |\n");
printf(" \\  /\\  / |___| |___| \\__/\\ \\_/ / |  | || |___     | | \\ \\_/ /    | | | | | || |___   | |/ /| |_| | |\\  | |_\\ \\| |___\\ \\_/ / |\\  |\n");
printf("  \\/  \\/\\____/\\_____/\\____/\\___/\\_|  |_/\\____/     \\_/  \\___/     \\_/ \\_| |_/\\____/   |___/  \\___/\\_| \\_/\\____/\\____/ \\___/\\_| \\_/\n");
 printf("\n");
}

void            afficher_vous_entrer_dans_le_donjon()
{
printf(" _____                        _                    _                _          _           _         \n");
printf("|  |  |___ _ _ ___    ___ ___| |_ ___ ___ ___    _| |___ ___ ___   | |___    _| |___ ___  |_|___ ___ \n");
printf("|  |  | . | | |_ -|  | -_|   |  _|  _| -_|- _|  | . | .'|   |_ -|  | | -_|  | . | . |   | | | . |   |\n");
printf(" \\___/|___|___|___|  |___|_|_|_| |_| |___|___|  |___|__,|_|_|___|  |_|___|  |___|___|_|_|_| |___|_|_|\n");
printf("                                                                                        |___|       \n");
}

void afficher_message_de_felicitations_fin_de_partie()
{
  printf(" _____   _          _        _                   _                                                      _\n");
  printf("|  ___| | (_)    (_) |      | | (_)             | |                                                    | |\n");
  printf("| |_ ___| |_  ___ _| |_ __ _| |_ _  ___  _ __   | |_ _   _    __ _ ___    __ _  __ _  __ _ _ __   ___  | |\n");
  printf("|  _/ _ \\ | |/ __| | __/ _` | __| |/ _ \\| '_ \\  | __| | | |  / _` / __|  / _` |/ _` |/ _` | '_ \\ / _ \\ | |\n");
  printf("| ||  __/ | | (__| | || (_| | |_| | (_) | | | | | |_| |_| | | (_| \\__ \\ | ( | | ( | | ( | | | | | |__/ |_|\n");
  printf("|_| \\___|_|_|\\___|_|\\__\\__,_|\\__|_|\\___/|_| |_|  \\__|\\__,_|  \\__,_|___/  \\__, |\\__,_|\\__, |_| |_|\\___| (_)");
  printf("                                                                                        __/ |       __/ |               ");
  printf("                                                                                       |___/       |___/                ");
}
