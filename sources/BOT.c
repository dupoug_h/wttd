#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "BOT.h"
#include "EFFETS.h"
#include "EQUIPEMENTS.h"


void                noms_des_bots(char* nom_du_bot)
{
    srand(time(NULL));
    switch (rand() % 12 )
    {
    case 1:
        strcpy(nom_du_bot, "M DUBEGNY");
        break;
    case 2:
        strcpy(nom_du_bot, "M ROY");
        break;
    case 3:
        strcpy(nom_du_bot, "M BOUTAMINE");
        break;
    case 4:
        strcpy(nom_du_bot, "M CESCHIA");
        break;
    case 5:
        strcpy(nom_du_bot, "M DECK");
        break;
    case 6:
        strcpy(nom_du_bot, "M HELART");
        break;
    case 7:
        strcpy(nom_du_bot, "M MOREUIL");
        break;
    case 8:
        strcpy(nom_du_bot, "M CAMENEN");
        break;
    case 9:
        strcpy(nom_du_bot, "M LEE");
        break;
    case 10:
        strcpy(nom_du_bot, "M HANOTEL");
        break;
    case 11:
        strcpy(nom_du_bot, "Mme PENAS");
        break;
    case 12:
        strcpy(nom_du_bot, "M ZEGGWAH");
        break;
    default : strcpy (nom_du_bot, "TU ME BOT");
    }
}
void                initialiser_le_bot(Joueur* bot, unsigned int position_du_joueur)
{
    bot->id = position_du_joueur;
    bot->nombre_de_victoires_du_donjon = 0;
    bot->passer_le_tour = JOUEUR_JOUE_ENCORE;
    bot->suis_je_un_bot = JE_SUIS_UN_BOT;
    noms_des_bots(bot->pseudo);
    printf("\nVous jouez avec le bot : %s. Il faudra l'aider quelques fois \n",bot->pseudo);
}

void        executer_le_bot(Donjon* donjon, Joueur* bot, PiocheMonstre* pioche_monstre, Aventurier* aventurier)
{
   unsigned int nombre_de_monstres_dans_le_donjon = 0;

    for (unsigned int index_monstre_donjon = 0; index_monstre_donjon < NOMBRE_DE_MONSTRES; index_monstre_donjon++)
    {

    if  (donjon->cartes_monstres[index_monstre_donjon] != NULL)
        nombre_de_monstres_dans_le_donjon++;
    }
    if (nombre_de_monstres_dans_le_donjon > 4)
        bot->passer_le_tour = JOUEUR_A_PASSER;
    else
    {
        piocher_une_carte_bot(pioche_monstre, bot);
        choix_action_apres_pioche_bot(donjon, bot, aventurier);
    }
}


void        piocher_une_carte_bot(PiocheMonstre* pioche_monstre, Joueur* bot)
{
    Monstre* monstre = piocher_un_monstre(pioche_monstre);
    bot->monstre_dans_ma_main = monstre;
}

void                choix_action_apres_pioche_bot(Donjon* donjon, Joueur* bot, Aventurier* aventurier)
{
    unsigned int    nombre_de_monstres_dans_le_donjon = 0;

    for (unsigned int index_monstre_donjon = 0; index_monstre_donjon < NOMBRE_DE_MONSTRES; index_monstre_donjon++)
    {
        if  (donjon->cartes_monstres[index_monstre_donjon] != NULL)
            nombre_de_monstres_dans_le_donjon++;
    }
    if (nombre_de_monstres_dans_le_donjon < 4)
        mettre_carte_dans_le_donjon(bot, donjon);
    else
        retirez_monstre_et_equipement_bot(aventurier);
}

int                 bot_du_donjon(Donjon* donjon, Aventurier* aventurier, PiocheMonstre* pioche_monstre)
{
    unsigned int    choix_effet = 0;

    for (unsigned int index_effet_monstre_a_enlever = 0; index_effet_monstre_a_enlever < NOMBRE_MAX_EFFETS; index_effet_monstre_a_enlever++)
    {
        if (aventurier->effets[index_effet_monstre_a_enlever] != NULL)
        {
            printf(" _______ Voici les diff�rents �quipements, veuillez selectionner les v�tres __ \n");
            printf("|                                                                             |\n");
            printf("|   1.  Anneau de pouvoir                                                     |\n");
            printf("|   2.  Cape d'invisibilit�                                                   |\n");
            printf("|   3.  Dague Vorpale                                                         |\n");
            printf("|   4.  Hache Vorpale                                                         |\n");
            printf("|   5.  Marteau de guerre                                                     |\n");
            printf("|   6.  Torche                                                                |\n");
            printf("|   7.  Pacte avec le d�mon                                                   |\n");
            printf("|   8.  Omnipotence                                                           |\n");
            printf("|   9.  Polymorphe                                                            |\n");
            printf("|   10. Saint graal                                                           |\n");
            printf("|   11. Lance dragon                                                          |\n");
            printf("|   12. Ep�e vorpale                                                          |\n");
            printf("|   13. Vous avez fini d'utiliser vos �quipements                             |\n");
            printf("|_____________________________________________________________________________|\n");
            choix_effet = lire_un_nombre_depuis_la_console("S�lectionnez un des �quipements du bot : ", "Le choix n'est pas valide\n", 1, 13);
            printf(" On a entre %d\n", choix_effet);
            switch (choix_effet)
            {
                case 1 : effet_anneau_de_pouvoir(donjon);
                    break;
                case 2 : effet_cape_invisibilite(donjon);
                    break;
                case 3 : effet_dague_vorpale_epee_vorpale(donjon);
                    break;
                case 4 : effet_hache_vorpale(donjon);
                    break;
                case 5 : effet_marteau_de_guerre(donjon);
                    break;
                case 6 : effet_torche(donjon);
                    break;
                case 7 : effet_pacte_avec_le_demon(donjon);
                    break;
                case 8 :
                    return (effet_omnipotence(donjon));
                case 9 : effet_polymorphe(donjon, pioche_monstre);
                    break;
                case 10 : effet_saint_graal(donjon);
                    break;
                case 11 : effet_lance_dragon(donjon);
                    break;
                case 12 : effet_dague_vorpale_epee_vorpale(donjon);
                    break;
                case 13 : action_de_quitter_selection_des_effets();
                    break;
                default : printf("ERREUR : laissez ce pauvre programme tranquille svp\n");
            }
        }
    }
    return (0);
}


void   retirez_monstre_et_equipement_bot(Aventurier* aventurier)
{
    unsigned int   compteur_de_cartes_equipement = compter_les_equipements(aventurier->equipements);
    unsigned int   equipement_qui_va_etre_supprime = 0;

    srand(time(NULL));
    if (compteur_de_cartes_equipement == 0)
        retirer_un_effet_de_aventurier_bot(aventurier);
    else
    {
        if (compteur_de_cartes_equipement == 1)
        {
            while (aventurier->equipements[equipement_qui_va_etre_supprime] == NULL)
                equipement_qui_va_etre_supprime++;
        }
        else
        {
            equipement_qui_va_etre_supprime = rand() % compteur_de_cartes_equipement;
            while (aventurier->equipements[equipement_qui_va_etre_supprime] == NULL)
                equipement_qui_va_etre_supprime = rand() % compteur_de_cartes_equipement;
        }
        printf("Le bot a retir� l'�quipement %s\n\n",aventurier->equipements[equipement_qui_va_etre_supprime]->nom);
        aventurier->equipements[equipement_qui_va_etre_supprime] = NULL;
    }
}

void        retirer_un_effet_de_aventurier_bot(Aventurier* aventurier)
{
    unsigned int   compteur_de_cartes_effets = compter_les_effets(aventurier->effets);
    unsigned int   effet_qui_va_etre_supprime = 0;

    srand(time(NULL));
    if (compteur_de_cartes_effets == 0)
        printf("ERREUR: L'aventurier n'a plus d'effets.\n\n");
    else
    {
        if (compteur_de_cartes_effets == 1)
        {
            while (aventurier->effets[effet_qui_va_etre_supprime] == NULL)
                effet_qui_va_etre_supprime++;
        }
        else
        {
            effet_qui_va_etre_supprime = rand() % compteur_de_cartes_effets;
            while (aventurier->effets[effet_qui_va_etre_supprime] == NULL)
                effet_qui_va_etre_supprime = rand() % compteur_de_cartes_effets;
        }
        printf("Le bot a retir� l'effet %s\n\n",aventurier->effets[effet_qui_va_etre_supprime]->nom);
        aventurier->effets[effet_qui_va_etre_supprime] = NULL;
    }
}
